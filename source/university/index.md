---
layout: markdown_page
title: University
---

## What is GitLab University

Version control will be the standard for creation of any type of media in the
future. Today it's still pretty hard to master.

GitLab University has as a goal to teach Git, GitLab and everything that relates
to that to anyone willing to listen and participate.

## Classes

Right now, there is a GitLab University class every Thursday at 5PM UTC.
To sign up, send Job a message.

In the future we want to have multiple classes:

- Lectures on new and exciting things
- Git 101
- GitLab 101
- Advanced GitLab
- Infrastructures and Enterprise

If you're interested in any of these, would like to teach in one of them or
participate or help in any way, please contact Job or submit a merge request.

### Recordings

[Internal Recordings](https://drive.google.com/drive/u/0/folders/0B41DBToSSIG_NlNFLUEwQ2JHSVk) link accessible to GitLab employees only

### Test results

Link to test results here.

## Resources

- [GitLab documentation](http://doc.gitlab.com/)
- [Innersourcing article](https://about.gitlab.com/2014/09/05/innersourcing-using-the-open-source-workflow-to-improve-collaboration-within-an-organization/)
- [Platzi training course](https://courses.platzi.com/courses/git-gitlab/)
- [GitLab flow](http://doc.gitlab.com/ee/workflow/gitlab_flow.html)
- [Sales Onboarding materials](https://about.gitlab.com/handbook/sales-onboarding/)
- [GitLab Direction](https://about.gitlab.com/direction/)
- [GitLab compared to other tools](https://about.gitlab.com/comparison/)