---
layout: markdown_page
title: Team Handbook
---

<!---
This page used to be HTML only, please convert to markdown when you touch it.
Please make headers linkable, for an example see the feedback header below.
-->

## Feedback<a name="feedback"></a>

Please make a <a href="https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests">merge request</a> to suggest improvements or add clarifications.
Please use <a href="https://gitlab.com/gitlab-com/www-gitlab-com/issues">issues</a> to ask questions.

## Subjects discussed on other pages of the handbook

<ul>
  <li><a href="/handbook/general-onboarding">General onboarding</a></li>
  <li><a href="/handbook/sales-onboarding">Sales onboarding</a></li>
  <li><a href="/handbook/developer-onboarding">Developer onboarding</a></li>
  <li><a href="/handbook/new-hire-day-one-us-employees-only">New hire day one (US employees only)</a></li>
  <li><a href="/handbook/human-resources">Human Resources</a></li>
  <li><a href="/handbook/travel">Travel</a></li>
  <li><a href="/handbook/sales-process">Sales process</a></li>
  <li><a href="/handbook/operations">Operations</a></li>
  <li><a href="/handbook/hiring">Hiring Process</a></li>
  <li><a href="/handbook/accounting">Accounting</a></li>
  <li><a href="/handbook/social-media">Social Media</a></li>
</ul>

## Contents of this page

<ul>
  <li><a href="#general-guidelines">General Guidelines</a></li>
  <li><a href="#communication">Communication</a></li>
  <li><a href="#intellectual-property">Intellectual Property</a></li>
  <li><a href="#spending-company-money">Spending Company Money</a></li>
  <li><a href="#paid-time-off">Paid Time Off</a></li>
  <li><a href="#signing-legal-documents">Signing Legal Documents</a></li>
  <li><a href="#general-guidelines">GitLab workflow guidlines</a></li>
  <li><a href="#starting-with-git">Using Git to update this website</a></li>
</ul>

<h2 id="general-guidelines">General Guidelines</h2>
  <ol>
    <li>We value results, transparency, sharing, freedom, efficiency, collaboration, directness, kindness, diversity, quirkiness, boring solutions, and interesting people.</li>
    <li>We try to channel our inner Ben Horowitz by being [both straightforward and kind, an uncommon cocktail of no-bullshit and no-asshole](https://medium.com/@producthunt/ben-horowitz-s-best-startup-advice-7e8c09c8de1b).
    <li>Working at GitLab Inc. is cooperating with the most talented people you've ever worked with, being the <b>most productive</b> you'll ever be, creating software that is helping the most people you've ever reached.</li>
    <li>We recognize that inspiration is perishable, if you’re <b>enthusiastic</b> about something that generates great results in relatively little time feel free to work on that.</li>
    <li>Do what is right for our <b>customers</b> and the rest of the GitLab community, do what is best over the long term, don't be evil.</li>
    <li>We create <b>simple</b> software to accomplish complex collaborative tasks.</li>
    <li>Do <b>not</b> make jokes or unfriendly remarks about race, ethnic origin, skin colour, gender or sexual orientation.</li>
    <li>Use <b>inclusive</b> language. For example, prefer 'Hi everybody' or 'Hi people' to 'Hi guys'.</li>
    <li>Share problems you run into, ask for help, be forthcoming with information and <b>speak up</b>.</li>
    <li>Work out in the <b>open</b>, try to use public issue trackers and repositories when possible.</li>
    <li>Most things are public unless there is a reason not to, not public by default are: financial information, job applications/compensation/feedback and partnerships with other companies.</li>
    <li>Share solutions you find and answers you receive with the <b>whole community</b>.</li>
    <li>If you make a mistake, don't worry, correct it and <b>proactively</b> let the affected party, your team, and the CEO know what happened, how you corrected it and how, if needed, you changed the process to prevent future mistakes.</li>
    <li>You can always <b>refuse</b> to deal with people that treat you badly and get out of situations that make you feel uncomfortable.</li>
    <li>Everyone can <b>remind</b> anyone in the company about the above points. If there is a disagreement about the interpretations, the discussion can be escalated to more people within the company without repercussions.</li>
    <li>If you are unhappy with anything (your duties, your colleague, your boss, your salary, your location, your computer) please let your boss,or the CEO, know as soon as you realize it. We want to solve problems while they are <b>small</b>.</li>
    <li>We want to have a great company so if you meet people that are <b>better</b> than yourself try to recruit them to join the company.</li>
    <li>It is easier to bond with people that have the same constraints, compensate for this and recognize that sales is hard because you are dependent on another organization and development is hard because you have to preserve the ability to quickly improve the product in the future.</li>
    <li>Our strategy is detailed in the <a href="https://docs.google.com/a/gitlab.com/document/d/1tco4-arhiA3V-XUJAkTtJ6zrfvT0OrUHtID2S1qVsXE/edit">GitLab Strategy document</a>, please read it and contribute.</li>
    <li>Explicitly note what <b>next action</b> you propose or expect and from who.</li>
    <li>There is no need for <b>consensus</b>, make sure that you give people that might have good insights a chance to respond (by /cc'ing them) but make a call yourself, <a href="https://twitter.com/sama/status/585950222703992833">consensus doesn't scale</a>.</li>
    <li>If you need to discuss with a team member for help please realize that probably the majority of the community also doesn't know, be sure to <b>document</b> the answer to radiate this information to the whole community. After the question is answered discuss where it should be documented and who will do it. You can remind other people of this by asking where/who?</li>
    <li>If you fix something for GitLab.com or one of our users make sure to make that the default (preferred) and radiate the information in the docs, we should run GitLab.com with the default settings and setup our users would also have.</li>
    <li>Be <b>specific</b> in your written communication, especially externally. Do not reply with a generalization when you can give an example.</li>
    <li>Be <b>kind</b> in your written communication, especially externally. Be constructive instead of argumentative or condescending.</li>
    <li>Everyone at the company cares about your <b>output</b>. Being away from the keyboard during the workday, doing private browsing or making personal phone calls is fine and encouraged.</li>
    <li>We're a <b>distributed</b> company where people work remote without missing out, prefer communication in public issues and chat channels and ensure conclusions of offline conversations are written down.</li>
    <li>Everything is always in draft and subject to change, including this handbook. So do not delay documenting things and do not include draft in the titles of documents. Ensure everyone can read the current state, nothing will ever be finished.</li>
    <li>If you copy content please remove it at the origin place and replace it with a link to the new content. Duplicate content leads to updating it in the wrong place, keep it [DRY](https://en.wikipedia.org/wiki/Don%27t_repeat_yourself).</li>
    <li>All guidelines in this handbook are meant to help, don't be afraid to do something because you can't oversee all guidelines, be gentle when reminding people about these guidlines, for example say: "It is not a problem, but next time please consider the following guideline from the handbook".</li>
  </ol>

<h2 id="communication">Communication</h2>

<h3 id="internal-communication">Internal Communication</h3>
<ol>
    <li>All written communication happens in English, even when sent one on one, because sometimes you need to forward an email or chat.</li>
    <li>Use <b>asynchronous communication</b> when possible (issues and email instead of chat), issues are preferred over email, email is preferred over chat.</li>
    <li>It is very OK to ask as many questions as you have, but ask them so many people can answer them and many people see the answer (so use issues or public chat channels instead of private messages or one-on-one emails) and make sure you try to document the answers.</li>
    <li>If you have to use email please send one email per subject as multiple items in one email will cause delays (have to respond to everything) or misses (forgot one of the items).</li>
    <li>Always reply to emails, even when no action is needed. This lets the other person know that you received it. A thread is done when there is a single word reply, such as OK, thanks, or done.</li>
    <li>If you forward an email without other comments please add FYI (for your information) or FYA (for your action).</li>
    <li>Emails sent to one person (no cc:) and  likely not include other people later on don't need a subject line (make the first line descriptive since Gmail uses that).</li>
    <li>If you use Slack, please use a public channel whenever possible, mention the person you want to reach if it is urgent. This ensures it is easy for other people to chime in, and easy to involve other people, if needed.</li>
    <li>All company data should be <b>shareable</b> by default. Don't use a local text file but leave comments on an issue. Create Google docs with your company Google Apps account. By default share Google docs with the whole company 'anyone at GitLab can find and access' with edit (preferred) or comment access for everyone. An easy way to do this, is to create your Google docs in a Shared Folder in Google Drive.</li>
    <li>All our procedures and templates are stored in (mostly public) git repositories instead of Google/Word documents. This makes them easier to find and suggest changes to with the organization and shows our commitment to open collaboration outside the organization.</li>
    <li>Use FYI and /cc if you want to inform people but it does not require an action, use FYA (for your action) if an action of the recipient is required.</li>
    <li>If there are materials relevant for a calendar meeting (for example a Google Doc) please add the url to the meeting invite description.</li>
    <li>If you want to ask people if they are available for an event please send a calendar appointment from and to the company address. This makes it easier for people to check availability and to put on their calendar. It automatically shows up on calendars even when the email is not opened. It is easier to signal presence and to see the status of everyone. Please respond quickly to invites so people can make plans.</li>
    <li>If you want to schedule time with a teammember for an outside meeting, create a calendar appointment and invite the teammember only, after they respond yes then invite outside people.</li>
    <li>If you want to move a meeting just move the calendar appointment instead of reaching out via other channels, note the change at the top of the description.</li>
    <li>
      Thank people that did a great job in our 'Thanks' chat channel.
      If someone is an employee just "@" mention them.
      If multiple people were working on something try mentioning each person by "@" name. 'Thanks everyone' does not say much.
      If someone is not an employee mention our office manager, their name, a quirky gift and link to their work.
      For example: <i>"@sylvia Joe deserves a lawnmower for LINK"</i>.
      The office manager will approach the people for their address saying we want to send some swag.
      We'll ship it in gift wrap with "Thanks for your great work on LINK, love from @gitlab".
      Don't thank the CEO or other executives for something that the company paid for, thank GitLab instead.
    </li>
</ol>

<h3 id="gitlab-workflow">GitLab Workflow</h3>
<ol>
    <li>Always <b>create</b> an issue for things you work on. If it is worth spending time on it is worth creating an issue for it since that enables other people to learn and help. You can always edit the description or close it when the problem is something different or disappears.</li>
    <li><b>'Double link'</b> issues to prevent internal confusion and us failing to report back to the reporters. For example, open an issue with link to ZenDesk and close the issue with copy of the response. Or add 'Report: ' lines to the description with links to relevant issues and feature requests and ensure they are closed and note this with a comment. If you are not responsible for reporting back please do not close an issue, instead reassign it.</li>
    <li>If issues are related <b>crosslink</b> them (a link from each issue to the other one). Put the links at the top of the issues' description with a short mention of the relationship (Report, etc.). If there are more than 2 issues use one issue as the central one and crosslink all issues to this one. Please, also crosslink between Zendesk and GitLab issues.</li>
    <li>Give the community the <b>chance to help</b>. For example: place issues on the feedback tracker, leave comments in rake check tests about what you can run manually and ask 'can you send a merge request to fix this?'.</li>
    <li>Submit the <b>smallest</b> item of work that makes sense. For example if you're new to GitLab and are writing documentation or instructions submit your first merge request for at most 20 lines.</li>
    <li>Do not leave issues open for a long time, issues should be <b>actionable</b> and realistic. If you are assigned but don't have time, assign it to someone else. If nobody is assigned and it is not a priority please ensure the community can help and close it.</li>
    <li>Make a conscious effort to <b>prioritize</b> your work. The priority of items depends on multiple factors: is there a team member waiting for the answer, what is the impact if you delay it, how many people does it affect, etc. What follows is a general guideline from urgent to less urgent:
    <ol>
        <li><b>Emergency</b> issues from standard or plus subscribers.</li>
        <li><b>Security</b> issues.</li>
        <li><b>Data integrity</b> (not losing data).</li>
        <li><b>Availability</b> of GitLab.com.</li>
        <li><b>Subscriber</b> questions.</li>
        <li><b>Regression</b> issues.</li>
        <li><b>Consultancy</b> work.</li>
        <li><b>Promised</b> features.</li>
        <li><b>Growth</b> efforts.</li>
        <li><b>Other</b> work.</li>
    </ol>
    </li>
    <li>Use the public issue trackers on GitLab.com for anything related to our software since <a href="https://about.gitlab.com/2015/08/03/almost-everything-we-do-is-now-open/">we work out in the open</a>.
    <li>Our internal dev.gitlab.org server is used for <a href="https://dev.gitlab.org/gitlab/gitlabhq/issues">security issues</a> (but do create a double linked tracking issue on GitLab.com), for <a href="https://dev.gitlab.org/cookbooks/chef-repo/issues">operations</a> and for <a href="https://dev.gitlab.org/gitlab/organization/issues">organizational</a> issues.
    <li>Pick issues from the current <a href="https://gitlab.com/groups/gitlab-org/milestones">milestone</a>.</li>
    <li>We try not to assign issues to people but to have people <b>pick issues</b> in a milestone themselves.</li>
    <li>Assign an issue to yourself as soon as you start to work on it, but not before that time.</li>
    <li>We keep our <b>promises</b> and do not make external promises without internal agreement.</li>
    <li>Even when something is not done, share it internally so people can comment early and prevent rework. Mark it <a href="http://doc.gitlab.com/ce/workflow/wip_merge_requests.html">WIP</a> so it is not merged by accident.</li>
    <li>If you are assigned to merge a Merge Request and there is a merge conflict, consider trying to resolve it <b>yourself</b> instead of asking the MR creator to resolve the conflict. If it is easy to resolve you avoid a round trip between you and the creator, and the MR gets merged sooner. This is a suggestion, not an obligation.</li>
    <li>When you create a merge request, please <b>mention</b> the issue that it solves in the description but avoid auto closing it by saying Fixes #1 or Solves #1. Since we frequently have to report back to customers or the rest of the community, issues are not done when the code is merged.</li>
    <li>If you ask a question to a specific person, always start the comment by mentioning them; this will ensure they see it if their notification level is mentioned and other people will understand they don't have to respond.</li>
    <li>Do not close an issue until code has been merged, it has been <b>reported</b> back to all parties, all issue trackers are updated and any documentation is written and merged.</li>
    <li>When <b>closing</b> an issue leave a comment explaining why you are closing the issue.</li>
</ol>

<h3 id="team-call">Team Call</h3>
<ol>
    <li>We use BlueJeans for the call since Hangouts is capped at 15 people, link is in the calendar invite.</li>
    <li>Please join the meeting with your x@gitlab.com account, not your private google one.</li>
    <li>We wait for people to join the first two minutes of the meeting until :32.</li>
    <li>We start by discussing the subjects that are on the agenda for today.</li>
    <li>When done with a point mention the subject of the next item and hand over to the next person.</li>
    <li>Sequence of asking people is in order of joining the company, same as on the [team page](https://about.gitlab.com/team/). If there are non-team page people in the call we end with those.</li>
    <li>We end with asking what they did after work the day(s) before and their work plans are for today/tomorrow.</li>
    <li>Last person hands over to the first person in the call order that is present (normally Dmitriy) who wishes everyone a good day.</li>
</ol>

<h3 id="user-communication-guidelines">User Communication Guidelines</h3>

<ol>
    <li>Keep conversations positive, friendly, real and productive while adding value.</li>
    <li>If you make a mistake, admit it. Be upfront and be quick with your correction. If you're posting to a blog, you may choose to modify an earlier post, just make it clear that you have done so.</li>
    <li>There can be a fine line between healthy debate and incendiary reaction. Try to frame what you write to invite differing points of view without inflaming others. You don’t need to respond to every criticism or barb. Be careful and considerate.</li>
    <li>Answer questions, thank people even if it’s just a few words. Make it a two way conversation.</li>
    <li>Appreciate suggestions and feedback.</li>
    <li>Don't make promises that you can't keep.</li>
    <li>Guide users who ask for help and share links. <a href="https://about.gitlab.com/2014/12/08/explaining-gitlab-bugs/">Types of requests</a>.</li>
    <li>If someone gives a suggestion, guide them to the <a href="http://feedback.gitlab.com/forums/176466-general">Feature Request Forum</a>.</li>
    <li>When facing negative comment respond patiently and treat every user as an individual, people with the strongest opinions can turn into <a href="https://about.gitlab.com/2015/05/20/gitlab-gitorious-free-software/">the strongest supporters</a>.</li>
</ol>

<h3 id="writing-style-guidelines">Writing Style Guidelines</h3>

<ol>
    <li>Do not use rich text, it makes it hard to copy/paste, use markdown instead.</li>
    <li>Do not create links like "here" or "click here". All links should have relevant anchor text that describes what they link to, such as: "GitLab CI source installation documentation".</li>
    <li>Always use <a href="http://xkcd.com/1179/">ISO dates</a> in all writing and legal documents, yyyy-mm-dd, e.g., 2015-04-13, and never 04-13-2015 or 13-04-2015</li>
    <li>If you have multiple points in a comment or email, please number them to make replies easier.</li>
    <li>When you reference an issue, merge request, comment, commit, page, doc, etc. and you have the url available please paste that in.</li>
    <li>In urls, always prefer hyphens to underscores.</li>
    <li>Always refer to 'the rest of the community', not 'the community' when referencing people outside the company since we're all in this together as one community.</li>
    <li>All people working for GitLab the company are the <a href="https://about.gitlab.com/team/">GitLab team</a>, we also have the <a href="https://about.gitlab.com/team/">Core team</a> that is part GitLab team, part volunteers.
    <li>Always write GitLab with a capitalized G and L, even when writing GitLab.com.</li>
    <li>When stating monetary amounts shouldn't have one digit, so prefer $19.90 to $19.9</li>
    <li>If an email needs a response write the ask at the top of it.</li>
    <li>Our homepage is https://about.gitlab.com/ (with the about. and with https).</li>
    <li>If you use headers properly format them (## in markdown, 'Heading 2' in Google docs), start at the second header level because header level 1 is for titles, do not end headers with a colon.</li>
    <li>Read our <a href="https://gitlab.com/gitlab-org/gitlab-ce/blob/master/doc_styleguide.md">Documentation Styleguide</a> for more information when writing documentation.</li>
</ol>

<h2 id="intellectual-property">Intellectual Property</h2>

<ol>
    <li>Take proper care of any <b>confidential</b> information you get from our customers.</li>
    <li>If you copy code always <b>check</b> the license and attribute when needed or appropriate.</li>
    <li>Check community <b>contributions</b> and do not merge it when there can be doubt about the ownership.</li>
    <li>Only the CEO of the company <b>signs</b> legal documents such as NDA’s. Sales people and the business office manager can upload them via Hellosign.</li>
 </ol>



<h2 id="spending-company-money">Spending Company Money</h2>

<ol>
    <li>Dutch employees will get their <b>salary</b> wired on the 25th of every month and will receive their salary slip by email on or before that date.</li>
    <li>Spend company money like it is your <b>own</b> money before taxes (since company expenses are not taxed you can spend it a bit more liberal than your own).</li>
    <li>You don't have to ask permission before making purchases in the interest of the company. When in doubt, do <b>inform</b> your manager before the purchase, or as soon as possible after the purchase.</li>
    <li><b>File</b> your expense report 7 days after the end of the calendar quarter.</li>
    <li>Any non-company expenses paid with a company credit card will have to be reported to your manager as soon as possible and <b>refunded</b> in full within 14 days.</li>
    <li><b>Mileage</b> is reimbursed according to local law: <a href="http://www.irs.gov/Tax-Professionals/Standard-Mileage-Rates">US rate per mile</a>, or <a href="http://www.belastingdienst.nl/wps/wcm/connect/bldcontentnl/belastingdienst/zakelijk/auto_en_vervoer/auto_van_de_onderneming/autokosten/u_rijdt_in_uw_eigen_auto">rate per km</a> in the Netherlands.</li>
    <li>The company will pay for the following <b>items</b> if you need it for work or use it mainly for business, and local law allows us to pay for it without incurring payroll taxes:
    <ul>
      <li>Laptop</li>
      <li>Laptop carrying bag</li>
      <li>External <a href="http://www.amazon.com/gp/product/B009C3M7H0?psc=1&redirect=true&ref_=oh_aui_detailpage_o04_s00">monitor</a>, <a href="http://www.amazon.com/Monoprice-32AWG-Mini-DisplayPort-Cable/dp/B0034X6SCY/ref=sr_1_1?ie=UTF8&qid=1442231319&sr=8-1&keywords=Monoprice+6ft+32AWG+Mini+DisplayPort+to+DisplayPort+Cable+-+White">monitor-cable</a>, keyboard, mouse, webcam and ethernet connector</li>
      <li>Headset</li>
      <li>(Height adjustable) desk</li>
      <li>Ergonomic chair</li>
      <li>Internet connection, for Dutch employees see <a href="https://docs.google.com/a/gitlab.com/document/d/1J70geARMCjRt_SfxIY6spdfpTbv_1v_KDeJtGRQ6JmM/edit#heading=h.5x5ssjstqpkq">Regeling Internet Thuis</a></li>
      <li>Mobile phone and subscription</li>
      <li>Telephone land line</li>
      <li>Skype calling credit, we can autofill your account</li>
      <li>Google Hangouts calling credit</li>
      <li>Office space (if working from home is not practical)</li>
      <li>Business travel upgrades:
        <ul>
            <li> Up to the first <a href="https://www.google.com/search?q=300+eur+in+usd">EUR 300</a> for an upgrade to Business Class on flights longer than 8 hours.</li>
            <li> Upgrade to Economy Plus if you’re taller than 1.95m / 6’5” for flights longer than 2 hours.</li>
            <li> Up to the first <a href="https://www.google.com/search?q=100+eur+in+usd">EUR 100</a> for an upgrade to Economy Plus (no height restriction) on flights longer than 8 hours.</li>
        </ul>
      </li>
      <li>Anything else you need? Please ask!</li>
      <li>To order please send a screenshot of what you need to purchasing@gitlab.com</li>
    </ul></li>
    <li><b>Expense Reimbursement</b> GitLab uses Expensify to facilitate the reimbursement of your expenses. You will receive an invitation by email to join GitLab's account. Please set up your account by following the instructions in the invitation.</li>
    <li><b> Expense Policy</b>
    <ul>
      <li>Max Expense Amount - 5,000 USD or 5,000 EUR</li> (per receipt, if you need more please ask in advance)
      <li>Receipt Required Amount - 25 USD or 25 EUR</li> (anything below can be filed without a receipt)
      <li>If you are new to Expensify and would like a brief review, please follow the links below</li>
      <a href="http://help.expensify.com/getting-started/">Getting Started</a></li>
</ol>


<h2 id="paid-time-off">Paid Time Off</h2>
  <ol>
    <li>Don't frown on people taking time off, but rather encourage that people take care of themselves and others.</li>
    <li>Working hours are flexible, you are invited for the team call if you are available, it is every 
    workday from 8:30am to 9:00am Pacific Time (mostly 5:30pm - 6:00pm Central European Time).</li>
    <li>You don't need to worry about taking time off to go to the gym, go grocery shopping, doing household chores, 
        helping someone, taking care of a loved one, etc. If something comes up or takes longer than expected and 
        you have urgent tasks and you're able to communicate, just ensure the rest of the team <b>knows</b> and someone can pick up any urgent tasks.</li>
    <li> We have an "unlimited" time off policy. This means that:
        <ul>
        <li> You do not need to ask permission to take time off unless you want to 
        take more than 25 consecutive calendar days.</li>
        <li> Always make sure that your job responsibilities are covered while you are away.</li>
        <li> We strongly recommended to take at least a minimum of 2 weeks of vacation per year, 
        if you take less your manager might follow up to discuss your work load.</li>
        </ul>
    </li>
    <li>You do need to ensure that not more than <b>half</b> of the people that can help with availability emergencies (24/7), 
    regular support, sales or development are gone at any moment. You can check for this on the 
    availability calendar, so be sure to add appointments early.</li>
    <li>If someone from the 24/7 team leaves, set up a <b>call</b> two weeks before that so the other people can divide the on-call days.</li>
    <li>Add an <b>appointment</b> to the GitLab availability calendar as you know your plans, you can always change it later.</li>
    <li>In case it can be useful add your planned time off as a <b>FYI</b> on the next agenda of the team call.</li>
    <li>We will <b>help</b> clients during official days off, unless they are official days off in both the Netherlands and the U.S. We try to have people working who are in a country that don't have an official day off. If you need to work during an official day off in your country, you should take a day off in return.</li>
  </ol>

<h2 id="signing-legal-documents">Signing Legal Documents</h2>
  If you need to sign, fill out, send or retrieve documents electronically, please send a copy of the document to legal@gitlab.com with the following information.</li>
  <ol>
        <li> Names of those who need to sign the document.</li>
        <li> Email addresses of any non-GitLab employees needing to sign the document.</li>
        <li> Any contractual information that needs to be included in the document.</li>
        <li> Deadline by which you need the document prepared.</li>
        <li> Deadline by which you need the document to be signed, dated and submitted.</li>
  </ol>
  The document will be managed through Hellosign, a cloud-based electronic signature tool.
  Only C level executives can sign legal documents, the only exception are NDA's covering a physical visit of another organization.

<h2 id="starting-with-git">Using Git to update this website</h2>

<h3>Start using GitLab</h3>
<ol>
   <li> Follow the step-by-step guides on the <a href="http://doc.gitlab.com/ce/gitlab-basics/README.html">basics of working with Git and GitLab.</a></li>
	<li>You will need to <a href="http://doc.gitlab.com/ce/gitlab-basics/create-your-ssh-keys.html">create your SSH Keys</a></li>
</ol>
<h3>Install Git</h3>
<ol>
    <li>Check Git version type: git --version </li>
    <li>If Git is not installed, you should get prompted to install it.</li>
</ol>
<h3>Install RVM</h3>
<ol>
    <li>Go to: <a href="https://rvm.io/">https://rvm.io</a></li>
    <li>Type: \curl -sSL https://get.rvm.io | bash -s stable</li>
    <li>Close Terminal</li>
    <li>Open new Terminal to load new environment</li>
</ol>
<h3>Install Ruby</h3>
<ol>
    <li> Type into Terminal: rvm install 2.2.1</li>
    <li> Prompt to enter password for your machine</li>
    <li> ruby --version</li>
    <li> bundle install</li>
    <li> gem install bundler</li>
</ol>
<h3>Preview website changes locally</h3>
<ol>
    <li> Type: bundle exec rake preview</li>
    <li> Type: http://localhost:4000</li>
    <li> To edit the HTML locally you need to install a text editior, we recommend <a href="http://www.sublimetext.com/2">Sublime.</a></li>
</ol>
